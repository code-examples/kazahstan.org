# [kazahstan.org](https://kazahstan.org) source codes

<br/>

### Run kazahstan.org on localhost

    # vi /etc/systemd/system/kazahstan.org.service

Insert code from kazahstan.org.service

    # systemctl enable kazahstan.org.service
    # systemctl start kazahstan.org.service
    # systemctl status kazahstan.org.service

http://localhost:4050
